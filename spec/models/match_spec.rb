require "rails_helper"



RSpec.describe Match, :type => :model do
  context 'validation tests' do

    let(:match) { build(:random_match) }

    it 'ensures date presence' do
    match.date = nil
    expect(match.save).to eq(false)
    end

    it 'ensures lat & lng presence' do
      match.latitude = nil
      match.longitude = nil
      expect(match.save).to eq(false)
    end

    it 'should save successfully' do
      expect(match.save).to eq(true)
    end


  end



end
