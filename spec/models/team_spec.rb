require 'rails_helper'

RSpec.describe Team, type: :model do
  context 'validation tests' do

    let(:team) { build(:random_team) }

    it 'ensures name presence' do
      team.name = nil
      expect(team.save).to eq(false)
    end

    it 'should save successfully' do
      expect(team.save).to eq(true)
    end
  end
end
