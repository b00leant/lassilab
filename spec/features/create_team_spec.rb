require 'rails_helper'

feature "signing up and in" do

  let(:team) {build(:random_team)}
  let(:user) {build(:user)}

  def fill_the_team_form
    p team.name
    fill_in "team[name]", with: team.name
    p "calcetto:"+ team.calcetto.to_s
    if team.calcetto
      find('label.checkbox_calcetto').click
    end
    p "calciotto:"+ team.calciotto.to_s
    if team.calciotto
      find('label.checkbox_calciotto').click
    end
    click_button "Crea Squadra"
  end

  def sign_up
    visit new_user_registration_path
    expect(page).to have_content("Sign up")
    p user.name
    fill_in "user[name]", with: user.name
    p user.email
    fill_in "user[email]", with: user.email
    p user.password
    fill_in "user[password]", with: user.password
    fill_in "user[password_confirmation]", with: user.password
    click_button "Sign up"
  end

  def sign_in
    visit new_user_session_path
    fill_in "user[email]", with: user.email
    p user.password
    fill_in "user[password]", with: user.password
    click_button "Log in"
  end

  scenario "clicking the 'new team in fab (floating action button)'" do
    Capybara.current_session.driver.header('Accept-Language','it')
    sign_up
    expect(page).to have_content("Logout")
    find("a.btn-floating[href='#{new_team_path}']").click
    fill_the_team_form
    expect(page).to have_content("Formazione")
  end

end
