require 'rails_helper'

feature "signing in and out" do
  let(:user) {build(:random_user)}

  def fill_in_signin_fields
    fill_in "user[email]", with: user.email
    #fill_in "user[name]", with: user.name
    fill_in "user[password]", with: user.password
    click_button "Log in"
  end

  scenario "clicking the logout button to log out" do
    visit root_path
    click_link "Login"
    fill_in_signin_fields
    click_link "Logout"
    expect(page).to have_content("Login")
  end

end
