# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

30.times do
  u = User.create(
    name: Faker::Name.name_with_middle,
    soccer_role: rand(0..4),
    password: "111111",
    password_confirmation: "111111",
    email: Faker::Internet.safe_email
  )
  rand(1..3).times do
    u.teams.create(
      name: Faker::Football.team,
      calcetto: [true, false].sample,
      calciotto: [true, false].sample
    )
  end
end
