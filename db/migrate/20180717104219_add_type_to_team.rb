class AddTypeToTeam < ActiveRecord::Migration[5.1]
  def change
    add_column :teams, :type, :integer
  end
end
