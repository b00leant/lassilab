class AddSoccerRoleToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :soccer_role, :integer
  end
end
