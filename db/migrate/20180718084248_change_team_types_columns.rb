class ChangeTeamTypesColumns < ActiveRecord::Migration[5.1]
  def change
    remove_column :teams, :type
    add_column :teams, :calcetto, :boolean
    add_column :teams, :calciotto, :boolean
  end
end
