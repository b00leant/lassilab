class SearchesController < ApplicationController
  def search

    @per_page = 10

    @teams = Team.where('lower(name) LIKE ?', "%#{params[:q].downcase}%")
    @users = User.where('lower(name) LIKE ?', "%#{params[:q].downcase}%")
    @free_matches = FreeMatch.where('lower(name) LIKE ?', "%#{params[:q].downcase}%")
    @users = @users.paginate(:page => params[:user_page], :per_page => @per_page)

    @teams = @teams.paginate(:page => params[:team_page], :per_page => @per_page)

    @free_matches = @free_matches.paginate(:page => params[:free_match_page], :per_page => @per_page)

    @collection = @teams + @users + @free_matches

    @q = params[:q]

    if params[:user_page].present?
      @user_page = params[:user_page].to_i
    else
      @user_page = 1
    end
    if params[:team_page].present?
      @team_page = params[:team_page].to_i
    else
      @team_page = 1
    end
    if params[:free_match_page].present?
      @free_match_page = params[:free_match_page].to_i
    else
      @free_match_page = 1
    end
  end

  def autocomplete_users
    @users = User.where('lower(name) LIKE ?', "%#{params[:q].downcase}%").limit(4) - User.where(id:current_user.id)

    render json: {
      "res" => @users
    }
  end
end
