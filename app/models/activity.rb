class Activity < ApplicationRecord
  #belongs_to :recipient, class_name: "User"
  belongs_to :actor, class_name: "User"
  belongs_to :object, polymorphic: true
end
