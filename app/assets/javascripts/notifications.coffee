# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
class Notifications
  constructor: ->
    @notifications = $("[data-behaviour='notifications']")
    @setup() if @notifications.length > 0

  setup: ->
    $.ajax(
      url: "/notifications.json"
      dataType: "JSON"
      method: "GET"
      success: @handleSuccess
    )
  handleSuccess: (data) =>
    if data.length > 0
      $("[data-behaviour='notifications']").append('<li
      class="center-align"><a class="subheader green
      darken-3 white-text">Notifiche</a></li><div style="margin:0" class="collection"
      data-behaviour="notifications-container">
      </div>')
      testitems = $.map data, (notification) ->
        if notification.notifiable.type is "Team" and notification.action is "invited"
          item = "<a href='#{notification.url}' class='collection-item avatar'>
          <i class='icon-soccer-shoe circle green darken-3 white-text' style='font-size:2em;color: white !important'></i>
          <span class='title'>Richiesta!</span>
          <p>#{notification.actor} ti ha invitato a far parte
          di una squadra</p></a>"
          $("[data-behaviour='notifications-container']").append(item)
        if notification.notifiable.type is "FreeMatch" and notification.action is "invited"
          item = "<a href='#{notification.url}' class='collection-item avatar'>
          <i class='fas fa-futbol circle white' style='color: black !important'></i>
          <span class='title'>Richiesta!</span>
          <p>#{notification.actor} ti ha invitato a un'amichevole'</p></a>"
          $("[data-behaviour='notifications-container']").append(item)
        if notification.notifiable.type is "FreeMatch" and notification.action is "requested"
          item = "<a href='#{notification.url}' class='collection-item avatar'>
          <i class='fas fa-futbol circle white' style='color: black !important'></i>
          <span class='title'>Richiesta!</span>
          <p>#{notification.actor} vuole partecipare alla tua partita amichevole</p></a>"
          $("[data-behaviour='notifications-container']").append(item)
        if notification.notifiable.type is "Match" and notification.action is "invited"
          item = "<a href='#{notification.url}' class='collection-item avatar'>
          <i class='icon-soccer-court circle white' style='color: green !important'></i>
          <span class='title'>Richiesta!</span>
          <p>La squadra #{notification.actor} desidera sfidarti in una partita</p></a>"
          $("[data-behaviour='notifications-container']").append(item)
    else
      $("[data-behaviour='notifications']").html('')
      $("[data-behaviour='notifications']").append('
      <li class="collection-item row center-align">
      <a class="subheader green
      darken-3 white-text">Notifiche</a>
      <div class="col s12 grey-text text-lighten-1">Non ci sono notifiche</div>
      <div class="col s12"><i class="material-icons grey-text text-lighten-1 large">notifications_off</i></div></li>
      ')

$(document).on "turbolinks:load", ->
    new Notifications
