class Follow
  constructor: ->
    @setupf()
  setupf: ->
    following_id = $("input.follow-input").data('following')
    follower_id = $("input.follow-input").data('follower')

    $("button.follow-button").click ->
      if $("button.follow-button").data('following') is 'ok'
        $.ajax(
          url: "/unfollow/#{following_id}/#{follower_id}"
          dataType: "JSON"
          method: "GET"
          success: (data) ->
            $("button.follow-button").html('<!--i class="material-icons left">add</i-->Segui').data('following','no')
        )

      if $("button.follow-button").data('following') is 'no'
        $.ajax(
          url: "/follow/#{following_id}/#{follower_id}"
          dataType: "JSON"
          method: "GET"
          success: (data) ->
            $("button.follow-button").html('<!--i class="material-icons left">check</i-->Segui già').data('following','ok')
            )




$(document).on "turbolinks:load", ->
  new Follow
