# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$(document).on "turbolinks:load", ->
  $('input.small.search-bar').focus ->
    $("div.sm-trigger").addClass("small");
    $("div.sm-trigger").removeClass("col");
    $("div.sm-trigger").removeClass("s2");
    $("div.sm-trigger").removeClass("m3");
    $('a.sidenav-trigger').hide()
  $('input.small.search-bar').focusout ->
    $('a.sidenav-trigger').show()
    $("div.sm-trigger").removeClass("small");
    $("div.sm-trigger").addClass("col");
    $("div.sm-trigger").addClass("s2");
    $("div.sm-trigger").addClass("m3");
  $('.clear-search-bar').click ->
    $('input.search-bar').val("")
  $('input.search-bar').focusout ->
    $('input.search-bar').val("")
